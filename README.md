## Starting project

```
npm init
node api.js
```

### Database

A dump of the database is available (dbexport.sql) or :

```
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_general_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
```

```
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `postId` int(11) NOT NULL,
  `body` varchar(255) NOT NULL,
  KEY `fk_foreign_postid` (`postId`),
  CONSTRAINT `fk_foreign_postid` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
Think to fix your database connection (l.34 in api.js): 

```
var connection = mysql.createConnection({
    host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : 'api'
})
```

## Routes - examples

- http://localhost:3000/posts/
- http://localhost:3000/posts/1
- http://localhost:3000/posts/1/comments
