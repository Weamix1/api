/* made with <3 with :
https://medium.com/@vsvaibhav2016/create-crud-application-in-express-js-9b88a5a94299
https://www.frugalprototype.com/developpez-propre-api-node-js-express/
https://dev.to/lenmorld/quick-rest-api-with-node-and-express-in-5-minutes-336j
*/

var express = require('express');
var app = express(); 
var bodyParser = require('body-parser')
var mysql = require("mysql");

var cors = require('cors'); // pour pouvoir utiliser l'api dans une page externe
app.use(cors());
 
app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
     res.setHeader("Access-Control-Allow-Credentials", "true");
     res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
     res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Origin,Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Authorization");
   next();
 });

app.use(bodyParser.urlencoded({
    extended: false
}))

app.use(bodyParser.json())

var hostname = 'localhost'; 
var port = 3000; 

var myRouter = express.Router(); 

var connection = mysql.createConnection({
    host     : 'localhost',
	user     : 'root',
	password : 'root',
	database : 'api'
})
connection.connect(function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('mysql connected');
    }
})

app.get('/posts/:id', function (req, res, next) {
    var id = req.params.id;
    console.log(id);
    var sql = 'SELECT * FROM ?? WHERE id = ?';
    var datas = ['posts', id];
    sql = mysql.format(sql, datas);
    connection.query(sql, function (err, result) {
        if (err) throw err;
        if (result.length > 0) {
            res.status(200).json({
                result
            });
        }
    });
});

app.get('/posts', function (req, res, next) {
    var sql = 'SELECT * FROM posts';
    connection.query(sql, function (err, result) {
        if (err) throw err;
        if (result.length > 0) {
            res.status(200).json({
                result
            });
        } else {
            res.status(200).json({
                'number': result.length
            });
        }
    });
});

app.post('/posts/', function (req, res, next) {
    var title = req.body.title;
    var body = req.body.body;
    var sql = 'INSERT INTO ?? (title, body) VALUES(?, ?)';
    var datas = ['posts', title, body];
    sql = mysql.format(sql, datas);
    connection.query(sql, function (err, rows) {
        if (err) throw err;
        res.status(200).json({
            'success': 'added'
        });
    })
});

app.delete('/posts/:id/', function (req, res, next) {
    var id = req.params.id;
    var sql = 'DELETE FROM ?? WHERE id = ?';
    var datas = ['posts', id];
    sql = mysql.format(sql, datas);
    connection.query(sql, function (err, result) {
        if (err) throw err;
        res.status(200).json({
            'delete': 'success'
        });

    });
});

app.put('/posts/:id', function (req, res, next) {
    var title = req.body.title;
    var body = req.body.body;
    var id = req.params.id;
    var sql = 'UPDATE ?? SET title = ?, body = ? WHERE id = ?';
    var datas = ['posts', title, body, id];
    sql = mysql.format(sql, datas);
    connection.query(sql, function (err, result) {
        if (err) throw err;
        res.status(200).json({
            result
        });

    });
});

app.get('/posts/:postId/comments/', function (req, res, next) {
    var id = req.params.postId;
    console.log(id);
    var sql = 'SELECT * FROM ?? WHERE id = ?';
    var datas = ['comments', id];
    sql = mysql.format(sql, datas);
    connection.query(sql, function (err, result) {
        if (err) throw err;
        if (result.length > 0) {
            res.status(200).json({
                result
            });
        }
    });
});

app.post('/posts/:postId/comments/', function (req, res, next) {
    var body = req.body.body;
    var sql = 'INSERT INTO ?? (body) VALUES(?)';
    var datas = ['comments', body];
    sql = mysql.format(sql, datas);
    connection.query(sql, function (err, rows) {
        if (err) throw err;
        res.status(200).json({
            'success': 'added'
        });
    })
});

app.use(myRouter);  
 
app.listen(port, hostname, function(){
	console.log("Serveur : http://"+ hostname +":"+port+"\n"); 
});